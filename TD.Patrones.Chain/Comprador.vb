﻿Imports TD.Patrones.Chain

Public Class Comprador
    Inherits Aprobador

    Public Overrides Sub Procesar(c As Compra)
        If c.Importe < 100 Then
            Console.WriteLine(String.Format("Compra aprobada por el {0}", Me.GetType.ToString))
        Else
            Me._siguiente.Procesar(c)
        End If
    End Sub
End Class
