﻿Public MustInherit Class Aprobador
    Protected _siguiente As Aprobador


    Public Sub AgregarSiguiente(aprobador As Aprobador)
        _siguiente = aprobador
    End Sub

    MustOverride Sub Procesar(c As Compra)
End Class
