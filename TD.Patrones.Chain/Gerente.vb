﻿Imports TD.Patrones.Chain

Public Class Gerente
    Inherits Aprobador

    Public Overrides Sub Procesar(c As Compra)
        If c.Importe <= 1000 Then
            Console.WriteLine(String.Format("Compra aprobada por el {0}", Me.GetType.ToString))
        Else
            Me._siguiente.Procesar(c)
        End If
    End Sub
End Class
