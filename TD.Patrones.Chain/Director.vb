﻿Imports TD.Patrones.Chain

Public Class Director
    Inherits Aprobador

    Public Overrides Sub Procesar(c As Compra)
        Console.WriteLine(String.Format("Compra aprobada por el {0}", Me.GetType.ToString))
    End Sub
End Class
