﻿Module Module1

    Sub Main()
        Dim comprador As New Comprador
        Dim gerente As New Gerente
        Dim director As New Director

        gerente.AgregarSiguiente(director)
        comprador.AgregarSiguiente(gerente)

        Dim c As New Compra
        Dim importe As Double = 1

        Do While importe <> 0
            Console.WriteLine("Ingrese el importe de la compra (0 para terminar)")
            importe = Double.Parse(Console.ReadLine())
            c.Importe = importe
            comprador.Procesar(c)
        Loop
    End Sub

End Module
